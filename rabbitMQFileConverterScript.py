import requests
import json
from bson import ObjectId
from datetime import datetime


studyID="5cf0f5f159af3164d759dd11"
moduleType="study"
exchange="file.converter.exchange"
key="file.converter.queue"

username="test"
password="test"
server="3.86.69.81"

videoList =[]
videoList.append("https://d3i3lk5iax7dja.cloudfront.net/youplus.fzsnf3drntqjzh278z4y.mp4")



def  publish(obId, videoUrl,studyID,moduleType,exchange,username,password,server,key):
    payload={}
    payload["ObjectID"]=str(obId)
    payload["videoUrl"]=videoUrl 
    payload["studyID"]=studyID
    payload["moduleType"]=moduleType  

    url     = "http://"+server+":15672/api/exchanges/%2f/"+exchange+"/publish"
    data = { "properties": {},
    "routing_key":key,
    "payload" : json.dumps(payload),
    "payload_encoding":"string"
    }

    res = requests.post(url, auth=(username, password),json=data)
    print(res)
    print(res.content)




for video in videoList:
    publish(ObjectId(),video,studyID,moduleType,exchange,username,password,server,key)
